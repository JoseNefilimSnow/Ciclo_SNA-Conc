import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailledviewPage } from './detailledview';

@NgModule({
  declarations: [
    DetailledviewPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailledviewPage),
  ],
})
export class DetailledviewPageModule {}
