import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransfertemplatePage } from './transfertemplate';

@NgModule({
  declarations: [
    TransfertemplatePage,
  ],
  imports: [
    IonicPageModule.forChild(TransfertemplatePage),
  ],
})
export class TransfertemplatePageModule {}
